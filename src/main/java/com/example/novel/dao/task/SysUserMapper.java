package com.example.novel.dao.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.novel.dao.modal.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}