package com.example.novel.dao.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.novel.dao.modal.SysDepartment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDepartmentMapper extends BaseMapper<SysDepartment> {

}