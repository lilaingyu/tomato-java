package com.example.novel.dao.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.novel.dao.modal.Task;
import org.apache.ibatis.annotations.Param;

public interface TaskMapper extends BaseMapper<Task> {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("id") Integer id, @Param("userId") String userId);

}