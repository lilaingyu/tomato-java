package com.example.novel.dao.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.novel.dao.modal.TaskNode;

public interface TaskNodeMapper  extends BaseMapper<TaskNode> {
}