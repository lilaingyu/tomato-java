package com.example.novel.dao.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable, UserDetails {

    private Integer id;

    private String password;

    private String phone;

    private String username;

    private String userCnName;

    private String portrait;

    private String dept;

    private String createby;

    private Date createtime;

    private String updateby;

    private Date updatetime;

    private List<GrantedAuthority> authorities;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;


    public Integer getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getDept() {
        return dept;
    }

    public String getCreateby() {
        return createby;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public String getUpdateby() {
        return updateby;
    }

    public Date getUpdatetime() {
        return updatetime;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    //默认使用恒等去判断是否是同一个对象，因为登录的同一个用户，如果再次登录就会封装
    //一个新的对象，这样会导致登录的用户永远不会相等，所以需要重写equals方法
    @Override
    public boolean equals(Object obj) {
        //会话并发生效，使用username判断是否是同一个用户

        if (obj instanceof User) {
            //字符串的equals方法是已经重写过的
            return ((User) obj).getUsername().equals(this.username);
        } else {
            return false;
        }
    }

}