package com.example.novel.dao.modal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "task_node")
public class TaskNode {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 主键
     */
    @TableField(value = "task_id")
    private String taskId;

    /**
     * 任务节点
     */
    @TableField(value = "task_node_name")
    private String taskNodeName;

    /**
     * 任务节点状态
     */
    @TableField(value = "task_node_state")
    private String taskNodeState;

    /**
     * 节点时间
     */
    @TableField(value = "task_node_starttime")
    private Date taskNodeStarttime;

    /**
     * 节点时间
     */
    @TableField(value = "task_node_endtime")
    private Date taskNodeEndtime;

    /**
     * 节点描述
     */
    @TableField(value = "task_node_desc")
    private String taskNodeDesc;

    /**
     * 0  默认  1 删除
     */
    @TableField(value = "is_delete")
    private int isDelete;

    @TableField(value = "createBy")
    private String createby;

    @TableField(value = "createTime")
    private Date createtime;

    @TableField(value = "updateBy")
    private String updateby;

    @TableField(value = "updateTime")
    private Date updatetime;

    public static final String COL_ID = "id";

    public static final String COL_TASK_ID = "task_id";

    public static final String COL_TASK_NODE_NAME = "task_node_name";

    public static final String COL_TASK_NODE_STATE = "task_node_state";

    public static final String COL_TASK_NODE_STARTTIME = "task_node_starttime";

    public static final String COL_TASK_NODE_ENDTIME = "task_node_endtime";

    public static final String COL_TASK_NODE_DESC = "task_node_desc";

    public static final String COL_IS_DELETE = "is_delete";

    public static final String COL_CREATEBY = "createBy";

    public static final String COL_CREATETIME = "createTime";

    public static final String COL_UPDATEBY = "updateBy";

    public static final String COL_UPDATETIME = "updateTime";
}