package com.example.novel.dao.modal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.novel.common.TaskUrgencyEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("task")
public class Task {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 任务标题
     */
    @JsonProperty("task_title")
    private String taskTitle;

    /**
     * 任务进度
     */
    @JsonProperty("task_status")
    private String taskStatus;

    /**
     * 任务进度
     */
    @JsonProperty("task_schedule")
    private BigDecimal taskSchedule;

    /**
     * 任务申请人
     */
    @JsonProperty("task_applicant")
    private String taskApplicant;

    /**
     * 任务申请人 名称
     */
    @JsonProperty("task_applicant_name")
    private String taskApplicantName;

    /**
     * 任务申请部门
     */
    @JsonProperty("task_application_department")
    private String taskApplicationDepartment;

    /**
     * 任务申请部门 名称
     */
    @JsonProperty("task_application_department_name")
    private String taskApplicationDepartmentName;

    /**
     * 任务申请类别
     */
    @JsonProperty("task_category")
    private String taskCategory;

    /**
     * 任务需求内容
     */
    @JsonProperty("task_content")
    private String taskContent;

    /**
     * 任务申请时间
     */
    @JsonProperty("task_startTime")
    private Date taskStarttime;

    /**
     * 紧急程度
     */
    @JsonProperty("urgency")
    private String urgency;

    /**
     * 任务参与者
     */
    @JsonProperty("task_participant")
    private String taskParticipant;

    /**
     * 任务参与者
     */
    @JsonProperty("task_participant_name")
    private String taskParticipantName;

    /**
     * 任务评估内容
     */
    @JsonProperty("task_estimate")
    private String taskEstimate;

    /**
     * 注释
     */
    @JsonProperty("notes")
    private String notes;

    /**
     * 任务预计完成时间
     */
    @JsonProperty("task_planned_completion_time")
    private Date taskPlannedCompletionTime;

    /**
     * 任务完工描述
     */
    @JsonProperty("task_finished_content")
    private String taskFinishedContent;

    /**
     * 任务结束时间
     */

    @JsonProperty("task_endTime")
    private Date taskEndtime;

    /**
     * 父任务
     */

    @JsonProperty("task_parent")
    private Integer taskParent;

    /**
     * 附件
     */

    @JsonProperty("annex")
    private String annex;

    /**
     * 0  默认  1 删除
     */

    @JsonProperty("is_delete")
    private int isDelete;

    /**
     * 创建者
     */

    @JsonProperty("createBy")
    private String createby;

    /**
     * 创建时间
     */
    @JsonProperty("createTime")
    private Date createtime;

    /**
     * 最后更新
     */
    @JsonProperty("updateBy")
    private String updateby;

    /**
     * 最后更新时间
     */
    @JsonProperty("updateTime")
    private Date updatetime;


    public void computeTaskStatus() {
        if (!Objects.isNull(this.getTaskSchedule())) {
            BigDecimal taskSchedule = this.getTaskSchedule();
            if (new BigDecimal("0").equals(taskSchedule)) {
                this.setTaskStatus(TaskUrgencyEnum.NotStarted.getStr());
            } else if (new BigDecimal("100").equals(taskSchedule)) {
                this.setTaskStatus(TaskUrgencyEnum.Ended.getStr());
                this.setTaskEndtime(new Date());
            } else {
                this.setTaskStatus(TaskUrgencyEnum.InProgress.getStr());
            }
        }
    }
}