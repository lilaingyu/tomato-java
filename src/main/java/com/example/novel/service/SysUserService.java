package com.example.novel.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.novel.common.CacheKeyConstant;
import com.example.novel.dao.modal.SysUser;
import com.example.novel.dao.task.SysUserMapper;
import com.example.novel.entity.SelectOptionObj;
import com.example.novel.util.JsonUtil;
import com.example.novel.util.RedisUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> {

    @Resource
    private RedisUtil redisUtil;

    public List<SelectOptionObj> getMptEmplyeeList(List<String> users) {

        if (redisUtil.hasKey(CacheKeyConstant.MPTEMPLYEEUSERLIST + users)) {
            String redisOrderStr = redisUtil.get(CacheKeyConstant.MPTEMPLYEEUSERLIST + users).toString();
            return JsonUtil.parseList(redisOrderStr, SelectOptionObj.class);
        }

        List<SysUser> list = this.lambdaQuery()
                .eq(SysUser::getIsDelete, 0)
                .isNotNull(SysUser::getUserNo)
                .isNotNull(SysUser::getUserName)
                .in(users != null, SysUser::getUserNo, users)
                .select(SysUser::getUserNo, SysUser::getUserName).list();

        List<SelectOptionObj> collect = list.stream().map((item) -> {
            String userId = item.getUserNo();
            String userNameCn = item.getUserName();
            return SelectOptionObj.builder().key(userId).value(userId).label(userNameCn).build();
        }).collect(
                collectingAndThen(
                        toCollection(() -> new TreeSet<>(Comparator.comparing(SelectOptionObj::getKey))), ArrayList::new)
        );

        if (!CollectionUtils.isEmpty(collect)) {
            if (!redisUtil.hasKey(CacheKeyConstant.MPTEMPLYEEUSERLIST + users)) {
                String cJson = JsonUtil.toJson(collect);
                redisUtil.set(CacheKeyConstant.MPTEMPLYEEUSERLIST + users, cJson, 60 * 30);
            }
        }

        return collect;

    }

}
