package com.example.novel.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.novel.dao.modal.TaskNode;
import com.example.novel.dao.task.TaskNodeMapper;
import com.example.novel.entity.PageObj;
import com.example.novel.util.Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class, value = "taskTransactionManager")
public class TaskNodeService extends ServiceImpl<TaskNodeMapper, TaskNode> {

    @Resource
    private TaskNodeMapper taskNodeMapper;


    public PageObj getTaskNotes(TaskNode taskNode, int page, int rows) {
        PageHelper.startPage(page, rows);
        PageInfo<TaskNode> pageInfo = new PageInfo<TaskNode>(taskNodeMapper.selectList(
                new LambdaQueryWrapper<TaskNode>()
                        .eq(TaskNode::getTaskId, taskNode.getTaskId())
                        .eq(TaskNode::getIsDelete, taskNode.getIsDelete()).orderByDesc(TaskNode::getCreatetime)
        ));
        return Utils.PageInfo2PageObj(pageInfo);
    }

    public List<TaskNode> getTaskAllNotes(TaskNode taskNode) {
        return taskNodeMapper.selectList(
                new LambdaQueryWrapper<TaskNode>()
                        .eq(TaskNode::getTaskId, taskNode.getTaskId())
                        .eq(TaskNode::getIsDelete, taskNode.getIsDelete()).orderByDesc(TaskNode::getCreatetime)
                        .last("limit 1000")
        );
    }

    public void taskNoteAdd(TaskNode taskNode) {
        this.saveOrUpdate(taskNode);
    }
}
