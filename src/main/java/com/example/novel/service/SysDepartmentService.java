package com.example.novel.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.novel.dao.modal.SysDepartment;
import com.example.novel.dao.task.SysDepartmentMapper;
import org.springframework.stereotype.Service;

@Service
public class SysDepartmentService extends ServiceImpl<SysDepartmentMapper, SysDepartment> {

}
