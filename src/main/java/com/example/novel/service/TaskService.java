package com.example.novel.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.novel.dao.modal.Task;
import com.example.novel.dao.task.TaskMapper;
import com.example.novel.entity.PageObj;
import com.example.novel.entity.SelectOptionObj;
import com.example.novel.util.RedisUtil;
import com.example.novel.util.Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class TaskService extends ServiceImpl<TaskMapper, Task> {

    @Resource
    private TaskMapper taskMapper;

    @Resource
    private SysUserService sysUserService;

    @Resource
    private RedisUtil redisUtil;


    public int deleteByPrimaryKey(Integer id, String userId) {
        return taskMapper.deleteByPrimaryKey(id, userId);
    }

    public PageObj getTasks(Task task, int page, int rows) throws IllegalAccessException {
        PageHelper.startPage(page, rows);

        PageInfo<Task> pageInfo = new PageInfo<Task>(
                taskMapper.selectList(
                        new LambdaQueryWrapper<Task>()
                                .eq(Task::getIsDelete, task.getIsDelete())
                                .eq(Objects.nonNull(task.getTaskParticipant()), Task::getTaskParticipant, task.getTaskParticipant())
                                .eq(Objects.nonNull(task.getTaskTitle()), Task::getTaskTitle, task.getTaskTitle())
                                .and(s -> s.eq(Task::getTaskApplicant, task.getTaskApplicant())
                                        .or(w -> w.eq(Task::getCreateby, task.getCreateby()))
                                        .or(m -> m.eq(Task::getTaskParticipant, task.getTaskParticipant())))
                )
        );
        return Utils.PageInfo2PageObj(pageInfo);
    }

    @Transactional(rollbackFor = Exception.class, value = "taskTransactionManager")
    public void taskSave(Task task) {




            if (!Objects.isNull(task.getTaskApplicant()) || !Objects.isNull(task.getTaskParticipant())) {
                List<String> participants = Arrays.asList(task.getTaskParticipant().split(","));
                List<String> taskApplicants = Arrays.asList(task.getTaskApplicant().split(","));

                List<String> users = new ArrayList<>();
                users.addAll(participants);
                users.addAll(taskApplicants);
                List<SelectOptionObj> selectOptionObjs = sysUserService.getMptEmplyeeList(users);

                String taskapplicatName = selectOptionObjs.stream().filter(s -> taskApplicants.contains(s.getValue())).map(SelectOptionObj::getLabel).collect(Collectors.joining(","));
                task.setTaskApplicantName(taskapplicatName);


                String taskparticipantName = selectOptionObjs.stream().filter(s -> participants.contains(s.getValue())).map(SelectOptionObj::getLabel).collect(Collectors.joining(","));
                task.setTaskParticipantName(taskparticipantName);
            }


        this.saveOrUpdate(task);
    }
}


