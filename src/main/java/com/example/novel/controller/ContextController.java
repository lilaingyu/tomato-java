package com.example.novel.controller;


import com.example.novel.common.BaseController;
import com.example.novel.dao.modal.SysUser;
import com.example.novel.entity.SelectOptionObj;
import com.example.novel.service.SysUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 * @author wzw
 */
@RestController
@RequestMapping("/api/context")
public class ContextController extends BaseController {

    @Resource
    private SysUserService sysUserService;

    /**
     * 用户下拉列表
     *
     * @return
     */
    @GetMapping("/mptEmployee/list")
    public List<SelectOptionObj> selectOne() {

        List<SysUser> list = sysUserService.lambdaQuery()
                .eq(SysUser::getIsDelete, 0)
                .isNotNull(SysUser::getUserNo)
                .isNotNull(SysUser::getUserName)
                .select(SysUser::getUserNo, SysUser::getUserName).list();

        List<SelectOptionObj> collect = list.stream().map((item) -> {
            String userId = item.getUserNo();
            String userNameCn = item.getUserName();
            return SelectOptionObj.builder().key(userId).value(userId).label(userNameCn).build();
        }).collect(
                collectingAndThen(
                        toCollection(() -> new TreeSet<>(Comparator.comparing(SelectOptionObj::getKey))), ArrayList::new)
        );

        return collect;
    }

}
