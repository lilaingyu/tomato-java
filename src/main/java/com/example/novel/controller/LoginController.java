package com.example.novel.controller;


import com.example.novel.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * LoginController
 *
 * @author Felordcn
 * @since 10 :05 2019/10/17
 */
@Slf4j
@RestController
@RequestMapping("/login")
public class LoginController {

    /**
     * 登录失败返回 401 以及提示信息. *
     *
     * @return the rest
     */
    @PostMapping("/failure")
    public Result loginFailure() {
        return Result.ERROR(HttpStatus.UNAUTHORIZED.value(), "登录失败");
    }


    /**
     * 登录成功后拿到个人信息. *
     *
     * @return the rest
     */
    @PostMapping("/success")
    public Result loginSuccess() {
        // 登录成功后用户的认证信息 UserDetails会存在 安全上下文寄存器 SecurityContextHolder 中
        org.springframework.security.core.userdetails.User principal =
                (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = principal.getUsername();

        return Result.SUCCESS("登录成功", username);
    }
}