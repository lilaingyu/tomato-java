package com.example.novel.controller;

import com.example.novel.common.BaseController;
import com.example.novel.common.Result;
import com.example.novel.dao.modal.SysUser;
import com.example.novel.dao.modal.TaskNode;
import com.example.novel.entity.PageObj;
import com.example.novel.entity.VO.TaskNodeVO;
import com.example.novel.service.TaskNodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * (photo.task_node)表控制层
 *
 * @author xxxxx
 */
@Slf4j
@RestController
@RequestMapping("/api/taskNote")
public class TaskNodeController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TaskNodeService taskNodeService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectTaskNoteById")
    public TaskNode selectOne(@RequestParam(required = false, value = "id") Integer id) {
        return taskNodeService.getById(id);
    }


    /**
     * 获取任务节点列表
     *
     * @param req
     * @param page   页数
     * @param rows   行数
     * @param taskId 任务id 必填
     * @return
     */
    @RequestMapping(value = "/getTaskNotes")
    public Result getTaskNotes(HttpServletRequest req, @RequestParam int page, @RequestParam int rows,
                               @RequestParam(required = false, value = "taskId") String taskId) {
        Result resp = new Result();
        try {
            // 构建查找的对象
            TaskNode taskNode = TaskNode.builder().taskId(taskId).isDelete(0).build();
            PageObj pageObj = taskNodeService.getTaskNotes(taskNode, page, rows);
            resp = Result.SUCCESS("查找成功", pageObj);
        } catch (Exception e) {
            log.error("Fail to getTaskNotes", e);
            resp = Result.ERROR("服务器错误");
        }
        return resp;
    }

    /**
     * 获取任务节点列表
     *
     * @param req
     * @param taskId 任务id 必填
     * @return
     */
    @RequestMapping(value = "/getTaskAllNotes")
    public Result getTaskAllNotes(HttpServletRequest req, @RequestParam(required = false, value = "taskId") String taskId) {
        Result resp = new Result();
        try {
            // 构建查找的对象
            TaskNode taskNode = TaskNode.builder().taskId(taskId).isDelete(0).build();
            List<TaskNode> taskAllNotes = taskNodeService.getTaskAllNotes(taskNode);
            resp = Result.SUCCESS("查找成功", taskAllNotes);
        } catch (Exception e) {
            log.error("Fail to getTaskNotes", e);
            resp = Result.ERROR("服务器错误");
        }
        return resp;
    }

    /**
     * 新增/修改
     *
     * @param taskNodeVO
     * @param request
     * @return
     */
    @RequestMapping(value = "/addTaskNote")
    public Integer addTask(@RequestBody TaskNodeVO taskNodeVO, HttpServletRequest request) {
        SysUser ue = getCurrentUser();
        TaskNode taskNode = new TaskNode();
        BeanUtils.copyProperties(taskNodeVO, taskNode);
        if (Objects.isNull(taskNode.getId())) {
            taskNode.setCreateby(ue.getUserNo());
        } else {
            taskNode.setUpdateby(ue.getUserNo());
        }
        taskNodeService.taskNoteAdd(taskNode);
        return taskNode.getId();
    }

    /**
     * 删除
     *
     * @param taskNoteId
     * @param request
     * @return
     */
    @RequestMapping(value = "/deleteTaskNote")
    public void addTask(@RequestParam int taskNoteId, HttpServletRequest request) {
        SysUser ue = getCurrentUser();
        TaskNode byId = taskNodeService.getById(taskNoteId);
        byId.setIsDelete(1);
        byId.setUpdateby(ue.getUserNo());
        taskNodeService.updateById(byId);

    }
}
