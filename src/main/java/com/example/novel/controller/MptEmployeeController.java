package com.example.novel.controller;


import com.example.novel.common.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wzw
 */
@Slf4j
@RestController
@RequestMapping("/api/employee")
public class MptEmployeeController  extends BaseController {

}
