package com.example.novel.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.novel.common.BaseController;
import com.example.novel.common.Result;
import com.example.novel.common.TaskUrgencyEnum;
import com.example.novel.dao.modal.SysUser;
import com.example.novel.dao.modal.Task;
import com.example.novel.entity.PageObj;
import com.example.novel.entity.VO.TaskVO;
import com.example.novel.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * (task)表控制层
 *
 * @author xxxxx
 */
@Slf4j
@RestController
@RequestMapping("/api/task")
public class TaskController extends BaseController {
    /**
     * 服务对象
     */
    @Resource
    private TaskService taskService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectTaskById")
    public Result selectOne(@RequestParam(required = false, value = "id") Integer id) {
        Task byId = taskService.getById(id);
        TaskVO task = new TaskVO();
        BeanUtils.copyProperties(byId, task);
        task.setTaskStatusDescribe(TaskUrgencyEnum.byCode(byId.getTaskStatus()).getDescribe());
        return Result.SUCCESS(task);
    }

    /**
     * 新增/修改 task
     *
     * @param taskVO
     * @param request
     * @return
     */
    @RequestMapping(value = "/addOrEditTask")
    public Integer addTask(@RequestBody TaskVO taskVO, HttpServletRequest request) {

        SysUser ue = getCurrentUser();
        Task task = new Task();
        BeanUtils.copyProperties(taskVO, task);

        if (Objects.isNull(task.getId())) {
            task.setTaskStatus(TaskUrgencyEnum.NotStarted.getStr());
            task.setTaskSchedule(new BigDecimal("0"));
            task.setCreateby(ue.getUserNo());
        } else {
            task.setUpdateby(ue.getUserNo());
            task.computeTaskStatus();
        }

        taskService.taskSave(task);
        return task.getId();
    }

    /**
     * 获取订单列表  , @RequestBody(required=false)  Order order
     */
    @RequestMapping(value = "/getTasks")
    public Result getTasks(HttpServletRequest req, @RequestParam int page, @RequestParam int rows,
                           @RequestParam(required = false, value = "taskParticipant") String taskParticipant, @RequestParam(required = false, value = "taskTitle") String taskTitle) {
        Result resp = new Result();
        try {
            SysUser ue = getCurrentUser();
            // 构建查找的order对象
            Task task = Task.builder().taskParticipant(taskParticipant)
                    .taskApplicant(ue.getUserNo())
                    .taskParticipant(ue.getUserNo())
                    .createby(ue.getUserNo())
                    .taskTitle(taskTitle).isDelete(0).build();
            PageObj pageObj = taskService.getTasks(task, page, rows);
            List collect = pageObj.getRows().stream().map((item) -> {
                System.out.println(item);
                TaskVO newTask = new TaskVO();
                BeanUtils.copyProperties(item, newTask);
                String taskStatus = newTask.getTaskStatus();
                newTask.setTaskStatusDescribe(TaskUrgencyEnum.byCode(taskStatus).getDescribe());
                return newTask;
            }).collect(Collectors.toList());

            pageObj.setRows(collect);

            resp = Result.SUCCESS("查找成功", pageObj);
        } catch (Exception e) {
            log.error("Fail to getTasks", e);
            resp = Result.ERROR("服务器错误");
        }
        return resp;
    }


    /**
     * 删除
     *
     * @param taskId
     * @param request
     * @return
     */
    @RequestMapping(value = "/deleteTask")
    public void addTask(@RequestParam int taskId, HttpServletRequest request) {
        SysUser ue = getCurrentUser();
        taskService.deleteByPrimaryKey(taskId, ue.getUserNo());
    }

    /**
     * 完善任务的各项数据
     * @param taskVO
     * @param request
     * @return
     */
    @RequestMapping(value = "/dataFillingTask")
    public boolean fillingTaskData(@RequestBody TaskVO taskVO, HttpServletRequest request) {

        SysUser ue = getCurrentUser();

        UpdateWrapper<Task> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", taskVO.getId());

        Task task = new Task();
        BeanUtils.copyProperties(taskVO, task);
        task.setUpdateby(ue.getUserNo());
        return taskService.update(task, updateWrapper);
    }

}
