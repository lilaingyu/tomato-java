package com.example.novel.common;


public class CacheTTLConstant {

    /**
     * 缓存1分钟
     */
    public static final String MINUTE= "minute:1";
    /**
     * 缓存30分钟
     */
    public static final String HALF_HOUR= "minute:30";
    /**
     * 缓存1天
     */
    public static final String DAY = "day:1";
    /**
     * 缓存30天
     */
    public static final String MONTH = "day:30";
    /**
     * 缓存1小时
     */
    public static final String DEFAULT = "hour:1";

    public static final String HOUR_2 = "hour:2";
    /**
     * 缓存5分钟
     */
    public static final String MINUTE_5 = "minute:5";
    /**
     * 缓存10分钟
     */
    public static final String MINUTE_10 = "minute:10";
    /**
     * 缓存15分钟
     */
    public static final String MINUTE_15 = "minute:15";
    /**
     * 缓存7天
     */
    public static final String WEEK = "day:7";
}
