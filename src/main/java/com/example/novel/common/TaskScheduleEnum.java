package com.example.novel.common;

public enum TaskScheduleEnum {
    /**
     * 紧急.
     */
    emergent,
    /**
     * 一般.
     */
    general,
    /**
     * 正常
     */
    normal
}
