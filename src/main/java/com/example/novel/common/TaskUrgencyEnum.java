package com.example.novel.common;

import java.util.Objects;

public enum TaskUrgencyEnum {

    NotStarted("NotStarted", "未开始"),
    InProgress("InProgress", "进行中"),
    Verifying("Verifying", "验证中"),
    Ended("Ended", "已结束"),
    UNDEFINED("UNDEFINED", "未知");


    private final String str;

    private final String describe;

    private TaskUrgencyEnum(String str, String describe) {
        this.str = str;
        this.describe = describe;
    }

    public String getStr() {
        return str;
    }

    public String getDescribe() {
        return describe;
    }

    public static TaskUrgencyEnum byCode(String code) {
        for (TaskUrgencyEnum value : values()) {
            if (Objects.equals(value.str, code)) return value;
        }
        return UNDEFINED;
    }


}
