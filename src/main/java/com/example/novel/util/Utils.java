package com.example.novel.util;

import com.example.novel.entity.PageObj;
import com.github.pagehelper.PageInfo;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;


public class Utils {

    public static final Logger logger = Logger.getLogger(Utils.class.getName());

    public static String[] chars = new String[]{"a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};

    public static  int getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) { //出生日期晚于当前时间，无法计算
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);  //当前年份
        int monthNow = cal.get(Calendar.MONTH);  //当前月份
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH); //当前日期
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;   //计算整岁数
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) {
                    age--;//当前日期在生日之前，年龄减一
                }
            }else{
                age--;//当前月份在生日之前，年龄减一
            } } return age; }

    //获取订单ID
    public static synchronized String getOrdersUUID() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "") + UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < 10; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        String time = new SimpleDateFormat("yyyyMMddHH").format(new Date());
        return time + shortBuffer.toString();
    }


    public static String randomUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }


    /**
     * 获取随机数6位
     *
     * @return
     */
    public static Integer getReadomCode() {
        return ((int) ((Math.random() * 9 + 1) * 100000));
    }


    /**
     * 获取文件存放目录
     *
     * @return
     */
    public static String getSavePath() {
        String path = new SimpleDateFormat("/yyyy/MM/dd").format(new Date());
        return "image" + path + "/";
    }

    /**
     * 解析以by为间隔的字符串，返回list<String>集合
     *
     * @param str 被分割的字符串
     * @param by  分割参数
     * @return 字符串集合list
     */
    public static List<String> splitStrtostr(String str, String by) {
        ArrayList<String> list = new ArrayList<String>();
        if (null != str && !"".equals(str)) {
            String[] arr = str.split(by);
            for (int i = 0; i < arr.length; i++) {
                list.add(arr[i].trim());
            }
        }
        return list;
    }


    /**
     * 支付宝订单号生成工具
     * 要求外部订单号必须唯一。
     *
     * @return
     */
    public static synchronized String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss", Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);
        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }

    /**
     * 从Request对象中获得客户端IP，处理了HTTP代理服务器和Nginx的反向代理截取了ip
     *
     * @param request
     * @return ip
     */
    public static String getLocalIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 获取当前时间戳，单位秒
     *
     * @return
     */
    public static long getCurrentTimestamp() {
        return System.currentTimeMillis() / 1000;
    }


    /**
     * 分页查询数据转换
     *
     * @param pageInfo com.github.pagehelper.PageInfo
     * @return hai.guo.novel.base.model.PageObj
     * <p>
     * {
     * "total":2,
     * "rows":[
     * {
     * "id":1,
     * "name":"张索尼"
     * }
     * ],
     * "pageNum":1,
     * "pageSize":1,
     * "pageToals":2
     * }
     */
    public static PageObj PageInfo2PageObj(PageInfo pageInfo) {
        PageObj pageObj = new PageObj();
        pageObj.setPageToals(pageInfo.getPages());
        pageObj.setRows(pageInfo.getList());
        pageObj.setTotal(pageInfo.getTotal());
        pageObj.setPageNum(pageInfo.getPageNum());
        pageObj.setPageSize(pageInfo.getPageSize());
        return pageObj;
    }

    public static String getTomcatPath() {
        String rootPath = new File("").getAbsolutePath();
        //返回tomcat真实路径   Users/a/soft/apache-tomcat-8.5.37/
        return rootPath.substring(0, rootPath.lastIndexOf("/") + 1);
    }

    public static <T> Map<String, Object> convertEntityToMap(T entity) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        // 获取对象的类
        Class<?> clazz = entity.getClass();
        // 获取类中的所有字段
        Field[] fields = clazz.getDeclaredFields();
        // 遍历字段，将字段名和值存入Map
        for (Field field : fields) {
            // 设置字段可访问（非public字段需要设置访问权限）
            field.setAccessible(true);
            // 获取字段名
            String fieldName = field.getName();
            // 获取字段值
            Object fieldValue = field.get(entity);
            if (Objects.isNull(fieldValue))
                continue;
            // 将字段名和值存入Map
            map.put(fieldName, fieldValue);
        }
        return map;
    }


}
