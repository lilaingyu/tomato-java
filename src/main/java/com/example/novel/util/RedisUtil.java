package com.example.novel.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.*;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author web
 * @date 2021/05/08
 */
public class RedisUtil {

    private final static Logger log = LoggerFactory.getLogger(RedisUtil.class);

    private final RedisTemplate<String, Object> redisTemplate;

    public RedisUtil(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * zSet 操作
     *
     * @return ZSetOperations
     */
    public ZSetOperations<String, Object> zSetOperations() {
        return redisTemplate.opsForZSet();
    }

    /**
     * set 操作
     *
     * @return SetOperations
     */
    public SetOperations<String, Object> setOperations() {
        return redisTemplate.opsForSet();
    }

    /**
     * list 操作
     *
     * @return ListOperations
     */
    public ListOperations<String, Object> listOperations() {
        return redisTemplate.opsForList();
    }

    /**
     * geo 操作
     *
     * @return GeoOperations
     */
    public GeoOperations<String, Object> geoOperations() {
        return redisTemplate.opsForGeo();
    }

    /**
     * HyperLogLog 操作
     *
     * @return HyperLogLogOperations
     */
    public HyperLogLogOperations<String, Object> hyperLogLogOperations() {
        return redisTemplate.opsForHyperLogLog();
    }

    /**
     * value 操作
     *
     * @return ValueOperations
     */
    public ValueOperations<String, Object> valueOperations() {
        return redisTemplate.opsForValue();
    }


    // =============================common============================

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete((Collection<String>) CollectionUtils.arrayToList(key));
            }
        }
    }

    // ============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : valueOperations().get(key);
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key, Object value) {
        try {
            valueOperations().set(key, value);
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }

    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                valueOperations().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key, Object value, long time, TimeUnit times) {
        try {
            if (time > 0) {
                valueOperations().set(key, value, time, times);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 递增 适用场景： https://blog.csdn.net/y_y_y_k_k_k_k/article/details/79218254 高并发生成订单号，秒杀类的业务逻辑等。。
     *
     * @param key 键
     * @return
     */
    public long incr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        return valueOperations().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     * @return
     */
    public long decr(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return valueOperations().increment(key, -delta);
    }

    // ================================Map=================================

    /**
     * HashGet
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒)
     * @return true成功 false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     * 并且设置过期时间，仅设置第一次
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false失败
     */
    public boolean hsetExpireOnce(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                if (redisTemplate.getExpire(key) < 0) {
                    expire(key, time);
                }
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以使多个 不能为null
     */
    public void hdel(String key, Object... item) {
        redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true 存在 false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key  键
     * @param item 项
     * @param by   要增加几(大于0)
     * @return
     */
    public double hincr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key  键
     * @param item 项
     * @param by   要减少记(小于0)
     * @return
     */
    public double hdecr(String key, String item, double by) {
        return redisTemplate.opsForHash().increment(key, item, -by);
    }

    // ============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> sGet(String key) {
        try {
            return setOperations().members(key);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return setOperations().isMember(key, value);
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object... values) {
        try {
            return setOperations().add(key, values);
        } catch (Exception e) {
            log.error(key, e);
            return 0;
        }
    }

    /**
     * 获取一个集合与给定集合的差集的元素
     *
     * @param key1 第一个键
     * @param key2 第二个键
     * @return set列表
     */
    public Set<Object> sSetDifference(String key1, String key2) {
        try {
            return setOperations().difference(key1, key2);
        } catch (Exception e) {
            log.error("key错误:" + key1 + key2, e);
            return new HashSet<>();
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key    键
     * @param time   时间(秒)
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = setOperations().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            log.error(key, e);
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return setOperations().size(key);
        } catch (Exception e) {
            log.error(key, e);
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            Long count = setOperations().remove(key, values);
            return count;
        } catch (Exception e) {
            log.error(key, e);
            return 0;
        }
    }

    // ============================zSet=============================

    /**
     * 在key处为已排序的集合添加值，或者如果它已经存在，则更新它的分数。
     *
     * @param key   键
     * @param value 值
     * @param score 分数
     * @return true: 操作成功 false: 操作失败
     */
    public Boolean zSAdd(String key, Object value, double score) {
        try {
            return zSetOperations().add(key, value, score);
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 移除指定键中的值
     *
     * @param key    键
     * @param values 多个值
     * @return 移除值的数量
     */
    public Long zSRemove(String key, Object... values) {
        try {
            return zSetOperations().remove(key, values);
        } catch (Exception e) {
            log.error(key, e);
            return 0L;
        }
    }

    /**
     * 确定一个排序集中带值元素的索引。
     *
     * @param key   键
     * @param value 值
     * @return 值在zSet中的排名
     */
    public Long zSRank(String key, Object value) {
        try {
            return zSetOperations().rank(key, value);
        } catch (Exception e) {
            log.error(key, e);
            return 0L;
        }
    }

    /**
     * 确定排序集中值元素的索引，当得分从高到低时。
     *
     * @param key   键
     * @param value 值
     * @return 值在zSet中的反向排名
     */
    public Long zSReverseRank(String key, Object value) {
        try {
            return zSetOperations().reverseRank(key, value);
        } catch (Exception e) {
            log.error(key, e);
            return 0L;
        }
    }

    /**
     * 从排序集中获取起始和结束之间的元素。
     *
     * @param key   键
     * @return set集合
     */
    public Set<Object> zSRange(String key) {
        try {
            return zSetOperations().range(key, 0, -1);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 从排序集中获取起始和结束之间的元素。
     *
     * @param key   键
     * @param start
     * @param end
     * @return set集合
     */
    public Set<Object> zSRange(String key, long start, long end) {
        try {
            return zSetOperations().range(key, start, end);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 从排序集中获取起始和结束之间的元素。
     *
     * @param key   键
     * @param start
     * @param end
     * @return set集合
     */
    public Set<Object> zSReverseRange(String key, long start, long end) {
        try {
            return zSetOperations().reverseRange(key, start, end);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 从排序集中获取分数在最小和最大之间的元素。
     *
     * @param key 键
     * @param min 最小分数
     * @param max 最大分数
     * @return set集合
     */
    public Set<Object> zSRangeByScore(String key, double min, double max) {
        try {
            return zSetOperations().rangeByScore(key, min, max);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 从排序集中获取从开始到结束范围内分数在最小和最大之间的元素。
     *
     * @param key 键
     * @param min 最小分数
     * @param max 最大分数
     * @return set集合
     */
    public Set<Object> zSReverseRangeByScore(String key, double min, double max) {
        try {
            return zSetOperations().reverseRangeByScore(key, min, max);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }


    /**
     * Returns the number of elements of the sorted set stored with given key.
     *
     * @param key 键
     * @return zSet的长度
     */
    public Long zSSize(String key) {
        try {
            return zSetOperations().size(key);
        } catch (Exception e) {
            log.error(key, e);
            return 0L;
        }
    }


    // ===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始 0 是第一个元素
     * @param end   结束 -1代表所有值
     * @return
     * @取出来的元素 总数 end-start+1
     */
    public List<Object> lGet(String key, long start, long end) {
        try {
            return listOperations().range(key, start, end);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return listOperations().size(key);
        } catch (Exception e) {
            log.error(key, e);
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return listOperations().index(key, index);
        } catch (Exception e) {
            log.error(key, e);
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            listOperations().rightPush(key, value);
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            listOperations().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            listOperations().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            listOperations().rightPushAll(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            listOperations().set(key, index, value);
            return true;
        } catch (Exception e) {
            log.error(key, e);
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = listOperations().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            log.error(key, e);
            return 0;
        }
    }

    /* =============================== bitmap ================================ */

    /**
     * 设置指定key和偏移量的 bitmap
     *
     * @param key    bitmap的key
     * @param offset 该bitmap的总长度
     * @return true 插入成功
     */
    public boolean setBit(String key, long offset) {
        return setBit(key, offset, true);
    }

    /**
     * 设置指定key和偏移量的 bitmap
     *
     * @param key    bitmap的key
     * @param offset 该bitmap的总长度
     * @param b      1(登录成功)/0(未登录)
     * @return true 插入成功
     */
    public boolean setBit(String key, long offset, boolean b) {
        return Boolean.TRUE.equals(valueOperations().setBit(key, offset, b));
    }

    /**
     * 获取指定bitmap key的偏移量
     *
     * @param key    bitmap的key
     * @param offset bitmap的偏移量 （获取用户在指定日期中是否登录）
     * @return true: 有记录值 false: 无记录值
     */
    public boolean getBit(String key, long offset) {
        return Boolean.TRUE.equals(valueOperations().getBit(key, offset));
    }
}
