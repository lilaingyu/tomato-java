package com.example.novel.entity.VO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskNodeVO implements Serializable {
    private Integer id;

    /**
     * 主键
     */
    @JsonProperty(value = "task_id")
    private String taskId;

    /**
     * 任务节点
     */
    @JsonProperty(value = "task_node_name")
    private String taskNodeName;

    /**
     * 任务节点状态
     */
    @JsonProperty(value = "task_node_state")
    private String taskNodeState;

    /**
     * 节点时间
     */
    @JsonProperty(value = "task_node_starttime")
    private Date taskNodeStarttime;

    /**
     * 节点时间
     */
    @JsonProperty(value = "task_node_endtime")
    private Date taskNodeEndtime;

    /**
     * 节点描述
     */
    @JsonProperty(value = "task_node_desc")
    private String taskNodeDesc;

    /**
     * 是否完成
     */
    @JsonProperty(value = "is_complete")
    private Integer isComplete;

    /**
     * 节点完成时间
     */
    @JsonProperty(value = "complete_time")
    private Date completeTime;
}