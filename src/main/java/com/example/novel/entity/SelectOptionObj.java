package com.example.novel.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SelectOptionObj {

	// 唯一键值
	private String key;

	// 值
	private String value;
	
	// 提示
	private String label;

}
