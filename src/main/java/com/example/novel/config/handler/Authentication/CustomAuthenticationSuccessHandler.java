package com.example.novel.config.handler.Authentication;

import com.example.novel.common.Result;
import com.example.novel.dao.modal.User;
import com.example.novel.service.SysUserService;
import com.example.novel.util.ResponseUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 自定义登录处理器
 *
 * @author Felordcn
 * @since 16:55 2019/10/23
 **/
@Slf4j
@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {


    @Autowired
    private SysUserService mptLsscSysUserService;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (response.isCommitted()) {
            System.out.println("Response has already been committed");
            return;
        }

        User principal = (User) authentication.getPrincipal();
        String username = principal.getUsername();
        // 存放返回对象
        Map<String, Object> map = new HashMap<>(10);

        Set<String> scopes = principal.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());

        String token = Jwts.builder()
                // 主题 放入用户名
                .setSubject(username)
                .setIssuer("novel")
                // 自定义属性 放入用户拥有请求权限
                .claim("scopes", scopes)
                // 失效时间
                .setExpiration(new Date(System.currentTimeMillis() + 7 * 60 * 60 * 1000 * 24))
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS256, "SigningKey")
                .compact();
        log.info("用户 {} 登录成功，为用户生成jwt：{}", username, token);
        map.put("access_token", token);

        map.put("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        map.put("flag", "success_login");
        map.put("userName", username);
        map.put("userCnName", principal.getUserCnName());
        map.put("dept", principal.getDept());


        ResponseUtil.responseJsonWriter(response, Result.SUCCESS("登录成功", map));
    }
}
