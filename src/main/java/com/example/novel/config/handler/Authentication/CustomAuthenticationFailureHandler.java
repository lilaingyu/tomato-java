package com.example.novel.config.handler.Authentication;

import com.example.novel.common.Result;
import com.example.novel.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义登录处理器
 *
 * @author Felordcn
 * @since 16:55 2019/10/23
 **/
@Slf4j
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        if (response.isCommitted()) {
            System.out.println("Response has already been committed");
            return;
        }
        Map<String, Object> map = new HashMap<>(2);

        map.put("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        map.put("flag", "failure_login");
        ResponseUtil.responseJsonWriter(response, Result.ERROR(HttpStatus.UNAUTHORIZED.value(), map, "认证失败"));
    }
}
