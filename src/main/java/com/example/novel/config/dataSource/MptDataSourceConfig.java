package com.example.novel.config.dataSource;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;


@Configuration
@MapperScan(basePackages = MptDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "mptSqlSessionFactory")
public class MptDataSourceConfig {
    static final String PACKAGE = "com.example.novel.dao.mpt";
    static final String MAPPER_LOCATION = "classpath*:mapper/*.xml";


    @Bean(name = "mptDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.mpt")
    public DataSource mptDataSource() {
        System.out.println("peo配置成功");
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "mptTransactionManager")
    public DataSourceTransactionManager mptTransactionManager() {
        return new DataSourceTransactionManager((mptDataSource()));
    }

    @Bean(name = "mptSqlSessionFactory")
    public MybatisSqlSessionFactoryBean mptSqlSessionFactory(@Qualifier("mptDataSource") DataSource mptDataSource) throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();
        sessionFactory.setDataSource(mptDataSource);
        sessionFactory.setMapperLocations(
                new PathMatchingResourcePatternResolver().getResources(MptDataSourceConfig.MAPPER_LOCATION)
        );
        return sessionFactory;
    }
}