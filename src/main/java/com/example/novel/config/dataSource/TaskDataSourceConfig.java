package com.example.novel.config.dataSource;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;


@Configuration
@MapperScan(basePackages = TaskDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "taskSqlSessionFactory")
public class TaskDataSourceConfig {
    // E:\XJBG\tomato-java\src\main\java\com\example\novel
    static final String PACKAGE = "com.example.novel.dao.task";
    static final String MAPPER_LOCATION = "classpath*:mapper/*.xml";

//    @Primary
    @Bean(name = "taskDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.task")
    public DataSource taskDataSource() {
        System.out.println("peo配置成功");
        return DataSourceBuilder.create().build();
    }

//    @Primary
    @Bean(name = "taskTransactionManager")
    public DataSourceTransactionManager taskTransactionManager() {
        return new DataSourceTransactionManager((taskDataSource()));
    }

//    @Primary
    @Bean(name = "taskSqlSessionFactory")
    public MybatisSqlSessionFactoryBean taskSqlSessionFactory(@Qualifier("taskDataSource") DataSource taskDataSource) throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();
        sessionFactory.setDataSource(taskDataSource);
        sessionFactory.setMapperLocations(
                new PathMatchingResourcePatternResolver().getResources(TaskDataSourceConfig.MAPPER_LOCATION)
        );
        return sessionFactory;
    }
}