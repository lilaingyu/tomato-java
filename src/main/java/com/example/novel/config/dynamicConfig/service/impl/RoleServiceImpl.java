package com.example.novel.config.dynamicConfig.service.impl;


import com.example.novel.config.dynamicConfig.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {
    @Override
    public Set<String> queryRoleByPattern(String pattern) {
        //todo  写你这里的逻辑  这里为了演示方便不使用数据库 这里遵循 ROLE_前缀
        //todo 你可以通过修改集合来模拟动态权限控制

        // 根据url查询可以访问该url的角色
        log.info("查询url：{} 的访问权限", pattern);
        Set<String> roles = new HashSet<>();
        roles.add("ROLE_ADMIN");
        return roles;
    }

    @Override
    public Set<String> queryAllAvailable() {
        Set<String> roles = new HashSet<>();

        System.out.println("queryAllAvailable查询所有的角色列表");

        roles.add("ROLE_ADMIN");

        return roles;
    }
}
