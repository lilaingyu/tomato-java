package com.example.novel.config.dynamicConfig;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * MetaResource
 *
 * @author Felordcn
 * @since 15:39 2019/11/28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MetaResource {
    private String pattern;
    private String method;
}
