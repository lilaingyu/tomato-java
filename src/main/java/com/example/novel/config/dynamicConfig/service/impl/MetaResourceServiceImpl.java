package com.example.novel.config.dynamicConfig.service.impl;



import com.example.novel.config.dynamicConfig.MetaResource;
import com.example.novel.config.dynamicConfig.service.MetaResourceService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MetaResourceServiceImpl implements MetaResourceService {

    @Override
    public Set<MetaResource> queryPatternsAndMethods() {

        Set<MetaResource> metaResources = new HashSet<>();
        metaResources.add(new MetaResource("/foo/test","GET"));
        metaResources.add(new MetaResource("/api/task/addOrEditTask","POST"));
        metaResources.add(new MetaResource("/api/task/getTasks","POST"));
        metaResources.add(new MetaResource("/api/task/selectTaskById","GET"));
        metaResources.add(new MetaResource("/api/context/mptEmployee/list","GET"));
        metaResources.add(new MetaResource("/api/taskNote/getTaskNotes","POST"));
        metaResources.add(new MetaResource("/api/taskNote/getTaskAllNotes","POST"));
        metaResources.add(new MetaResource("/api/taskNote/addTaskNote","POST"));
        metaResources.add(new MetaResource("/api/taskNote/deleteTaskNote","POST"));
        metaResources.add(new MetaResource("/api/taskNote/selectTaskNoteById","GET"));

        metaResources.add(new MetaResource("/api/task/dataFillingTask","POST"));

        return metaResources;
    }
}
