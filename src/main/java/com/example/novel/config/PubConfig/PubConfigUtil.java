package com.example.novel.config.PubConfig;

import com.example.novel.service.SysUserService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 启动项目后, 加载数据库公共配置数据到redis中
 */
@Component
public class PubConfigUtil {

    @Resource
    private SysUserService mptEmployeeService;

    /**
     * 是java注解，在方法上加该注解会在项目启动的时候执行该方法，也可以理解为在spring容器初始化的时候执行该方法。
     */
    @PostConstruct
    public void reload() {
        mptEmployeeService.getMptEmplyeeList(null);
    }
}