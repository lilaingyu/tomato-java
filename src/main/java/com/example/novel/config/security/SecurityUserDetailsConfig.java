package com.example.novel.config.security;

import com.example.novel.common.GendorException;
import com.example.novel.dao.modal.SysDepartment;
import com.example.novel.dao.modal.SysUser;
import com.example.novel.dao.modal.User;
import com.example.novel.service.SysDepartmentService;
import com.example.novel.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.annotation.Resource;
import java.util.List;

@Configuration
public class SecurityUserDetailsConfig {

    /**
     * 首先，pom里加了springsecurity的依赖后就会发现，它自动把你的应用保护起来了，需要输入用户名密码什么，并且再控制台输出了一个生成的密码
     * 这个默认的用户名和密码是由 UserDetailsServiceAutoConfiguration 生成，这个东西有 @Configuration 相关注解，说明了当前有什么类和been，并且当有了
     * AuthenticationManager.class, AuthenticationProvider.class, UserDetailsService.class
     * 这三个是当前的就不会生效。
     * <p>
     * UserDetailsServiceAutoConfiguration 里面有一个been   inMemoryUserDetailsManager() 他实现了 UserDetailsManager 而实现UserDetailsManager的作用就是规定security如何管理用户
     * 就是这个 inMemoryUserDetailsManager 的 been 将自动生成的账号（默认是user）密码（每次自动生成）加载到了内存中
     * <p>
     * 根据UserDetailsServiceAutoConfiguration的注解我们可以知道，但自己实现UserDetailsService并加载到been中试这个UserDetailsServiceAutoConfiguration就不会生效
     * 所以我们可以自己写一个 userDetailsManager 来管理用户（ userDetailsManager 实现了 UserDetailsService 接口）
     * <p>
     * 看下面的代码这里有两个been
     * userDetailsManager(UserDetailsRepository userDetailsRepository)     这里是通过内部匿名类的方式实现了userDetailsManager，他规定了怎么创建删除查找用户
     * userDetailsRepository 这个been是存放用户的地方，在初始化这个been的时候，就创建了一个『flordcn』的用户，
     * 一开始有点懵逼因为 UserDetailsServiceAutoConfiguration 是在创建 inMemoryUserDetailsManager 这个的时候传了一个进去，把UserDetailsManager的功能搞混了，
     * 后来才明白，UserDetailsManager 只是规定了如何操作用户，这里的主要体现试增删改查
     * 这里是将存放用户和操作用户的分成两个been inMemoryUserDetailsManager 是在一个been里完成的
     * <p>
     * 要实现数据库管理相关用户只需要把UserDetailsRepository中的users这个map换成查询数据库的Dao层接口就ok
     */

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Resource
    private SysUserService mptLsscSysUserService;

    @Resource
    private SysDepartmentService sysDepartmentService;


    @Bean
    public UserDetailsRepository userDetailsRepository() {
        UserDetailsRepository userDetailsRepository = new UserDetailsRepository();
        // 为了让我们的登录能够运行 这里我们初始化一个用户Felordcn 密码采用明文
//        // 当你在密码 12345上使用了前缀{noop} 意味着你的密码不使用加密，authorities 一定不能为空 这代表用户的角色权 限集合
//        UserDetails felordcn = User.withUsername("Felordcn").password(passwordEncoder.encode("123456")).authorities(AuthorityUtils.createAuthorityList("ADMIN")).build();
//        UserDetails felordcn1 = User.withUsername("Felordcn1").password(passwordEncoder.encode("123456")).authorities(AuthorityUtils.createAuthorityList("ADMIN")).build();
//        userDetailsRepository.createUser(felordcn);
//        userDetailsRepository.createUser(felordcn1);
        return userDetailsRepository;
    }


    /**
     * User details manager 自定义.
     *
     * @param userDetailsRepository the user details repository
     * @return the user details manager
     * @see org.springframework.security.provisioning.JdbcUserDetailsManager
     */
    @Bean
    public UserDetailsManager userDetailsManager(UserDetailsRepository userDetailsRepository) {
        return new UserDetailsManager() {
            @Override
            public void createUser(UserDetails user) {
//                userDetailsRepository.createUser(user);
            }

            @Override
            public void updateUser(UserDetails user) {
//                userDetailsRepository.updateUse.r(user);
            }

            @Override
            public void deleteUser(String username) {
//                userDetailsRepository.deleteUser(username);
            }

            @Override
            public void changePassword(String oldPassword, String newPassword) {
//                userDetailsRepository.changePassword(oldPassword, newPassword);
            }

            @Override
            public boolean userExists(String username) {
//                return userDetailsRepository.userExists(username);
                System.out.println("userExists");
                return true;
            }

            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

                List<SysUser> list = mptLsscSysUserService.lambdaQuery().eq(SysUser::getUserNo, username).list();
                // todo emm用户不存在怎么处理
                if (list.isEmpty())
                    throw new GendorException("用户不存在");
                SysUser lsscSysUser = list.get(0);
                User user = new User();
                user.setUsername(lsscSysUser.getUserNo());
                user.setPassword(lsscSysUser.getPassword());
                user.setUserCnName(lsscSysUser.getUserName());
                // 查询部门
                List<SysDepartment> dept = sysDepartmentService.lambdaQuery().eq(SysDepartment::getDeptNo, lsscSysUser.getDeptNo()).eq(SysDepartment::getIsDelete, 0).list();
                if (!dept.isEmpty())
                    user.setDept(dept.get(0).getDeptName());
                user.setAuthorities(AuthorityUtils.createAuthorityList("ROLE_ADMIN"));

                return user;
            }
        };
    }
}
