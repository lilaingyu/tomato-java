package com.example.novel.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * 密码配置类
 * 讲一下密码相关
 * 在 WebSecurityConfigurerAdapter 中这个类中定义了如何对用户密码进行编码保护
 * 其中有一个内部类名叫 LazyPasswordEncoder 的类，人如其名，这是个懒加载
 * 重要的是里面有一个方法 getPasswordEncoder() 根据代码可以知道他加载了一个been
 * 这个been就是PasswordEncoder，如果这个been是null就是用默认编码 默认是bcrypt编码
 * 所以我们自己注入一个PasswordEncoder的been就可以自定义使用那种编码
 *
 *
 *
 */
@Configuration
public class SecurityPasswordEncoderConfig {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
